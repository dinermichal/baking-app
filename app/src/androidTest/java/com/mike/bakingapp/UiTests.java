package com.mike.bakingapp;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.IdlingResource;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;

import com.mike.bakingapp.ui.Activities.MainActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

public class UiTests {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);


    private IdlingResource mIdlingResource;

    @Before
    public void registerIdlingResource() {
        mIdlingResource = mActivityTestRule.getActivity().getIdlingResource();
        Espresso.registerIdlingResources(mIdlingResource);
    }

    @Test
    public void checkText_MainActivity() {
        onView(withId(R.id.recycler_view_recipes_main)).perform(RecyclerViewActions.scrollToPosition(0));
        onView(withText("Nutella Pie")).check(ViewAssertions.matches(isDisplayed()));
        onView(withId(R.id.recycler_view_recipes_main)).perform(RecyclerViewActions.scrollToPosition(1));
        onView(withText("Brownies")).check(ViewAssertions.matches(isDisplayed()));
        onView(withId(R.id.recycler_view_recipes_main)).perform(RecyclerViewActions.scrollToPosition(2));
        onView(withText("Yellow Cake")).check(ViewAssertions.matches(isDisplayed()));
        onView(withId(R.id.recycler_view_recipes_main)).perform(RecyclerViewActions.scrollToPosition(3));
        onView(withText("Cheesecake")).check(ViewAssertions.matches(isDisplayed()));
    }

    @Test
    public void checkViewsInSteps_StepsDetailFragment() {
        onView(withId(R.id.recycler_view_recipes_main)).perform(RecyclerViewActions.actionOnItemAtPosition(0, ViewActions.click()));
        onView(withId(R.id.recycler_view_steps)).perform(scrollTo());
        onView(withId(R.id.recycler_view_steps)).perform(RecyclerViewActions.actionOnItemAtPosition(0, ViewActions.click()));
        onView(withId(R.id.step_video)).check(ViewAssertions.matches(isDisplayed()));
        onView(withId(R.id.long_desc)).check(ViewAssertions.matches(isDisplayed()));
    }

    @After
    public void unregisterIdlingResource() {
        if (mIdlingResource != null) {
            Espresso.unregisterIdlingResources(mIdlingResource);
        }
    }
}