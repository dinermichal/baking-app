package com.mike.bakingapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mike.bakingapp.R;
import com.mike.bakingapp.RecipeListener;
import com.mike.bakingapp.Steps;
import com.mike.bakingapp.ui.Activities.RecipeDetailActivity;
import com.mike.bakingapp.ui.Activities.StepsDetailActivity;
import com.mike.bakingapp.ui.Fragments.RecipeDetailFragment;

import java.util.List;

import static com.mike.bakingapp.utils.Static.DESC;
import static com.mike.bakingapp.utils.Static.ID;
import static com.mike.bakingapp.utils.Static.RECIPE_ID;
import static com.mike.bakingapp.utils.Static.VIDEO_URL;

public class StepsAdapter extends RecyclerView.Adapter<StepsAdapter.ViewHolder>{

    private final RecipeListener mCallback;
    private List<Steps> stepsList;
    private Context context;

    public StepsAdapter(Context context, List<Steps> stepsList, RecipeListener mCallback) {
        this.context = context;
        this.stepsList = stepsList;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public StepsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.steps_card, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull StepsAdapter.ViewHolder holder, final int position) {
        final Steps steps = stepsList.get(position);
        //String name, quantity, measure, ingredient, shortDesc, desc, videoUrl, thumbnailUrl, servings;
        holder.shortDesc.setText(steps.getShortDesc());
        holder.stepsCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Steps steps1 = stepsList.get(position);
                Intent detailIntent = new Intent (v.getContext(), StepsDetailActivity.class);
                detailIntent.putExtra(VIDEO_URL, steps1.getVideoUrl());
                detailIntent.putExtra(DESC, steps1.getDesc());
                detailIntent.putExtra(RECIPE_ID, RecipeDetailFragment.recipeId);
                detailIntent.putExtra(ID, steps1.getId());
                if (!RecipeDetailActivity.mTwoPane) {
                    v.getContext().startActivity(detailIntent);
                }
                else {
                    mCallback.onStepsClicked(steps1.getId(), RecipeDetailFragment.recipeId, steps1.getVideoUrl(), steps1.getDesc());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return stepsList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView shortDesc;
        LinearLayout stepsCard;


        ViewHolder(View itemView) {
            super(itemView);
            shortDesc = itemView.findViewById(R.id.step_desc);
            stepsCard = itemView.findViewById(R.id.steps_card);
        }

    }
}
