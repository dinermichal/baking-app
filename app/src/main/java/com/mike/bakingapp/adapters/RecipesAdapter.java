package com.mike.bakingapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mike.bakingapp.R;
import com.mike.bakingapp.Recipes;
import com.mike.bakingapp.ui.Activities.RecipeDetailActivity;

import java.util.List;

import static com.mike.bakingapp.utils.Static.ID;
import static com.mike.bakingapp.utils.Static.RECIPE_NAME;

public class RecipesAdapter extends RecyclerView.Adapter<RecipesAdapter.ViewHolder>{

    private List<Recipes> recipesList;
    private Context context;

    public RecipesAdapter(Context context, List<Recipes> recipesList) {
        this.context = context;
        this.recipesList = recipesList;
    }

    @NonNull
    @Override
    public RecipesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recipe_card, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecipesAdapter.ViewHolder holder, final int position) {
        final Recipes recipes = recipesList.get(position);
        //String name, quantity, measure, ingredient, shortDesc, desc, videoUrl, thumbnailUrl, servings;
        holder.recipeName.setText(recipes.getName());
        holder.servings.setText(recipes.getServings());
        holder.recipeCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Recipes recipes1 = recipesList.get(position);
                Intent detailIntent = new Intent (v.getContext(), RecipeDetailActivity.class);
                detailIntent.putExtra(ID, recipes1.getId());
                detailIntent.putExtra(RECIPE_NAME, recipes.getName());
                v.getContext().startActivity(detailIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return recipesList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView recipeName;
        TextView servings;
        LinearLayout recipeCard;

        ViewHolder(View itemView) {
            super(itemView);
            recipeName = itemView.findViewById(R.id.recipe_name);
            servings = itemView.findViewById(R.id.servings);
            recipeCard =itemView.findViewById(R.id.recipe_card);
        }

    }
}
