package com.mike.bakingapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mike.bakingapp.Ingredients;
import com.mike.bakingapp.R;

import java.util.List;

public class IngredientsAdapter extends RecyclerView.Adapter<IngredientsAdapter.ViewHolder>{
    private List<Ingredients> ingredientsList;
    private Context context;

    public IngredientsAdapter(Context context, List<Ingredients> ingredientsList) {
        this.context = context;
        this.ingredientsList = ingredientsList;
    }

    @NonNull
    @Override
    public IngredientsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.ingredients_card, parent, false);
        return new IngredientsAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull IngredientsAdapter.ViewHolder holder, final int position) {
        final Ingredients ingredients = ingredientsList.get(position);
        holder.quantity.setText(ingredients.getQuantity());
        holder.measure.setText(ingredients.getMeasure());
        holder.ingredient.setText(ingredients.getIngredient());
    }

    @Override
    public int getItemCount() {
        return ingredientsList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView quantity;
        TextView measure;
        TextView ingredient;

        ViewHolder(View itemView) {
            super(itemView);
            quantity = itemView.findViewById(R.id.quantity);
            measure = itemView.findViewById(R.id.measure);
            ingredient =itemView.findViewById(R.id.ingredient);
        }

    }
}
