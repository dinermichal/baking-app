package com.mike.bakingapp.widget;

import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.mike.bakingapp.R;

import java.util.ArrayList;

import static com.mike.bakingapp.widget.RecipeWidgetProvider.ingredientsList;

public class RecipeWidgetService extends RemoteViewsService {
    ArrayList <String> remoteViewList;

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new RecipeRemoteViewsFactory(this.getApplicationContext(), intent);

    }

    private class RecipeRemoteViewsFactory implements RemoteViewsFactory {
        private Context context = null;

        RecipeRemoteViewsFactory(Context applicationContext, Intent intent) {
            context = applicationContext;
        }



        @Override
        public void onCreate() {

        }

        @Override
        public void onDataSetChanged() {
        remoteViewList = ingredientsList;
        }

        @Override
        public void onDestroy() {

        }

        @Override
        public int getCount() {
            return remoteViewList.size();
        }

        @Override
        public RemoteViews getViewAt(int position) {
            final RemoteViews remoteView = new RemoteViews(
                    context.getPackageName(), R.layout.recipe_widget_list_item);
            remoteView.setTextViewText(R.id.widget_ingredients_list_item, remoteViewList.get(position));
            remoteView.setOnClickFillInIntent(R.id.widget_ingredients_list_item, new Intent());

            return remoteView;
        }

        @Override
        public RemoteViews getLoadingView() {
            return null;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }
    }
}

