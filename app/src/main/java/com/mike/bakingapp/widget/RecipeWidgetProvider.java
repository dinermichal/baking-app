package com.mike.bakingapp.widget;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.widget.RemoteViews;

import com.mike.bakingapp.R;

import java.util.ArrayList;
import java.util.Objects;

import static com.mike.bakingapp.utils.Static.INGREDIENTS_LIST;
import static com.mike.bakingapp.utils.Static.RECIPE_NAME;

public class RecipeWidgetProvider extends AppWidgetProvider {


    public static ArrayList<String> ingredientsList;

    public static void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId, String recipe) {

        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.recipe_widget);
        remoteViews.setTextViewText(R.id.widget_recipe_name, recipe);
        Intent i = new Intent(context, RecipeWidgetService.class);
        remoteViews.setRemoteAdapter(R.id.widget_list, i);
        appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
    }


    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

    }

    public static void onStaticUpdate (Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds, String recipeName) {
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId, recipeName);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
            ingredientsList = Objects.requireNonNull(intent.getExtras()).getStringArrayList(INGREDIENTS_LIST);
            String recipeName = intent.getExtras().getString(RECIPE_NAME);
            if (!TextUtils.isEmpty(recipeName)){
                AppWidgetManager.getInstance(context).notifyAppWidgetViewDataChanged(AppWidgetManager.getInstance(context)
                        .getAppWidgetIds(new ComponentName(context, RecipeWidgetProvider.class)), R.id.widget_list);

                onStaticUpdate(context, AppWidgetManager.getInstance(context),
                        AppWidgetManager.getInstance(context).getAppWidgetIds(new ComponentName(context, RecipeWidgetProvider.class))
                        , recipeName);
                super.onReceive(context, intent);
            }
        }


    @Override
    public void onEnabled(Context context) {
    }

    @Override
    public void onDisabled(Context context) {
    }
}

