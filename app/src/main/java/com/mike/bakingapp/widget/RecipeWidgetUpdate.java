package com.mike.bakingapp.widget;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.Objects;

import static com.mike.bakingapp.utils.Static.INGREDIENTS_LIST;
import static com.mike.bakingapp.utils.Static.RECIPE_NAME;
import static com.mike.bakingapp.utils.Static.WIDGET_ACTION;

public class RecipeWidgetUpdate extends IntentService {




    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     *  name Used to name the worker thread, important only for debugging.
     */
    public RecipeWidgetUpdate() {
        super("BakingApp");
    }

    public static void startWidgetUpdate (Context context, ArrayList<String> ingredientsList, String recipeName){
        Intent i = new Intent(context, RecipeWidgetUpdate.class);
        i.putExtra(INGREDIENTS_LIST, ingredientsList);
        i.putExtra(RECIPE_NAME, recipeName);
        context.startService(i);
    }
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        assert intent != null;
        broadcastIntent(Objects.requireNonNull(intent.getExtras()).getStringArrayList(INGREDIENTS_LIST),
                intent.getExtras().getString(RECIPE_NAME));
    }

    private void broadcastIntent(ArrayList<String> ingredientsList, String recipeName) {
        Intent i = new Intent (WIDGET_ACTION);
        i.setAction(WIDGET_ACTION);
        i.putExtra(INGREDIENTS_LIST, ingredientsList);
        i.putExtra(RECIPE_NAME, recipeName);
        sendBroadcast(i);
    }
}
