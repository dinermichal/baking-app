package com.mike.bakingapp.ui.Fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.mike.bakingapp.R;
import com.mike.bakingapp.data.RecipesContract;
import com.mike.bakingapp.ui.Activities.RecipeDetailActivity;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import static com.mike.bakingapp.utils.Static.DESC;
import static com.mike.bakingapp.utils.Static.ID;
import static com.mike.bakingapp.utils.Static.VIDEO_URL;

public class StepsDetailFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>{

    public static final String YELLOW_CAKE = "3";
    public static final boolean NEXT = true;
    public static final boolean PREVIOUS = false;
    public static final String STEPS_SELECTED = "steps_selected";
    public static final int NUTELLAPIE = 1;
    public static final int BROWNIES = 2;
    public static final int YELLOWCAKE = 3;
    public static final int CHEESECAKE = 4;
    public static final String VIDEO_POSITION = "VIDEO_POSITION";
    public static final String WHEN_READY = "WHEN_READY";
    @SuppressLint("StaticFieldLeak")
    private static TextView longDesc;
    private String desc;
    private Button nextBtn;
    private Button previousBtn;
    public int intStepsId;
    private String recipeId;
    public String stepsId;
    private SimpleExoPlayerView simpleExoPlayerView;
    private SimpleExoPlayer simpleExoPlayer;
    private DefaultBandwidthMeter bandwidthMeter;
    private ImageView noVideo;
    private int intRecipeId;
    private String videoUrl;
    private LinearLayout.LayoutParams layoutParams;
    private String userAgent;
    private long videoPosition;
    private boolean playWhenReady;

    public void setRecipeId(String recipeId) {
        this.recipeId = recipeId;
    }

    public void setStepsId(String stepsId) {
        this.stepsId = stepsId;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public StepsDetailFragment() {
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_step_detail, container, false);

        Intent i= Objects.requireNonNull(getActivity()).getIntent();
        nextBtn = rootView.findViewById(R.id.next);
        previousBtn = rootView.findViewById(R.id.previous);
        longDesc = rootView.findViewById(R.id.long_desc);
        simpleExoPlayerView = rootView.findViewById(R.id.step_video);
        userAgent = Util.getUserAgent(getContext(), "BakingApp");

        if (!RecipeDetailActivity.mTwoPane){
            nextBtn.setVisibility(View.VISIBLE);
            previousBtn.setVisibility(View.VISIBLE);
            stepsId = Objects.requireNonNull(i.getExtras()).getString(ID);
            videoUrl = i.getExtras().getString(VIDEO_URL);
            desc = Objects.requireNonNull(i.getExtras()).getString(DESC);

            if (getActivity().getResources().getConfiguration().orientation ==
                    Configuration.ORIENTATION_LANDSCAPE) {
                ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
                longDesc.setVisibility(View.GONE);
                layoutParams = (LinearLayout.LayoutParams)
                        simpleExoPlayerView.getLayoutParams();
                layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
                simpleExoPlayerView.setLayoutParams(layoutParams);
            }
            else if (getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
                ((AppCompatActivity) getActivity()).getSupportActionBar().show();
                longDesc.setVisibility(View.VISIBLE);
                layoutParams = (LinearLayout.LayoutParams)
                        simpleExoPlayerView.getLayoutParams();
                layoutParams.width= ViewGroup.LayoutParams.MATCH_PARENT;
                layoutParams.height= 600;
                simpleExoPlayerView.setLayoutParams(layoutParams);
            }
        }
        if (savedInstanceState != null){
            stepsId = savedInstanceState.getString(STEPS_SELECTED);
            desc = savedInstanceState.getString(DESC);
            videoUrl = savedInstanceState.getString(VIDEO_URL);
            playWhenReady = savedInstanceState.getBoolean(WHEN_READY);
        }
        longDesc.setText(desc);
        intStepsId = Integer.parseInt(stepsId);
        intRecipeId = Integer.parseInt(RecipeDetailFragment.recipeId);

        bandwidthMeter = new DefaultBandwidthMeter();

        simpleExoPlayerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
        noVideo = rootView.findViewById(R.id.no_video);
        if (TextUtils.isEmpty(videoUrl)){
            simpleExoPlayerView.setVisibility(View.GONE);
            noVideo.setVisibility(View.VISIBLE);
            Picasso.with(getContext()).load(R.drawable.no_video).into(noVideo);
        }
        else {
            simpleExoPlayerView.setVisibility(View.VISIBLE);
            noVideo.setVisibility(View.GONE);
            initializePlayer(Uri.parse(videoUrl));
            simpleExoPlayer.setPlayWhenReady(true);
            if (savedInstanceState !=null){
                videoPosition = savedInstanceState.getLong(VIDEO_POSITION);
            }
        }


        //Buttons
        buttonsCheck();

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fixYellowCakeJSON(NEXT);
                intStepsId++;
                getActivity().getSupportLoaderManager().restartLoader(0, null, StepsDetailFragment.this);
                buttonsCheck();

            }
        });
        previousBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fixYellowCakeJSON(PREVIOUS);
                intStepsId--;
                getActivity().getSupportLoaderManager().restartLoader(0, null, StepsDetailFragment.this);
                buttonsCheck();
            }
        });
        return rootView;
    }

    private void fixYellowCakeJSON(boolean next) {
        if (next){
            if (RecipeDetailFragment.recipeId.equals(YELLOW_CAKE) && intStepsId == 6){
                intStepsId++;
            }
        }
        else{
            if (RecipeDetailFragment.recipeId.equals(YELLOW_CAKE) && intStepsId == 8){
                intStepsId--;
            }
        }
    }

    @NonNull
    @Override
    public android.support.v4.content.Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] mSelectionArgs = {String.valueOf(intStepsId)};
        Uri NAME_URI = RecipesContract.RecipesEntry.CONTENT_URI;
        return new CursorLoader(Objects.requireNonNull(getContext()), NAME_URI, null,
                RecipesContract.RecipesEntry.RECIPES_STEP_ID + " = ?", mSelectionArgs, null);
    }

    @Override
    public void onLoadFinished(android.support.v4.content.Loader<Cursor> loader
            , Cursor mCursor) {
        if (mCursor != null && mCursor.getCount() >0) {
            mCursor.moveToPosition(-1);
            while(mCursor.moveToNext()) {
                desc = mCursor.getString(mCursor.getColumnIndex(RecipesContract.RecipesEntry.RECIPES_STEP_DESC));
                videoUrl = mCursor.getString(mCursor.getColumnIndex(RecipesContract.RecipesEntry.RECIPES_STEP_VIDEOURL));
                longDesc.setText(desc);
                if (TextUtils.isEmpty(videoUrl)) {
                    simpleExoPlayerView.setVisibility(View.GONE);
                    noVideo.setVisibility(View.VISIBLE);
                    Picasso.with(getContext()).load(R.drawable.no_video).into(noVideo);
                } else {
                    simpleExoPlayerView.setVisibility(View.VISIBLE);
                    noVideo.setVisibility(View.GONE);
                    if (simpleExoPlayer == null) {
                        initializePlayer(Uri.parse(videoUrl));
                    } else {
                        reloadPlayer(Uri.parse(videoUrl));
                    }
                }
            }
        }
    }

    private void reloadPlayer(Uri videoUrl) {

        MediaSource mediaSource = new ExtractorMediaSource(videoUrl,
                new DefaultDataSourceFactory(getContext(), userAgent),
                new DefaultExtractorsFactory(),
                null,
                null);
        simpleExoPlayer.prepare(mediaSource);
        simpleExoPlayer.setPlayWhenReady(true);
    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader loader) {

    }
    private void buttonsCheck() {
        switch (intRecipeId){
            case NUTELLAPIE:
                disableNextButton(6);
                disablePreviousButton();
                break;
            case BROWNIES:
                disableNextButton(9);
                disablePreviousButton();
                break;
            case YELLOWCAKE:
                disableNextButton(13);
                disablePreviousButton();
                break;
            case CHEESECAKE:
                disableNextButton(12);
                disablePreviousButton();
                break;

        }
    }

    private void disablePreviousButton() {
        if (intStepsId ==0){
            previousBtn.setEnabled(false);
        }
        else{
            previousBtn.setEnabled(true);
        }
    }

    private void disableNextButton(int maxNumberOfSteps) {
        if (intStepsId ==maxNumberOfSteps){
            nextBtn.setEnabled(false);
        }
        else{
            nextBtn.setEnabled(true);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (simpleExoPlayer !=null) {
            simpleExoPlayer.stop();
            simpleExoPlayer.release();
            simpleExoPlayer = null;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (simpleExoPlayer !=null) {
            videoPosition = simpleExoPlayer.getContentPosition();
            playWhenReady = simpleExoPlayer.getPlayWhenReady();
            simpleExoPlayer.stop();
            simpleExoPlayer.release();
            simpleExoPlayer = null;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (simpleExoPlayer !=null){
            simpleExoPlayer.setPlayWhenReady(playWhenReady);
            simpleExoPlayer.seekTo(videoPosition);
        }
        else{
            if (TextUtils.isEmpty(videoUrl)){
                simpleExoPlayerView.setVisibility(View.GONE);
                noVideo.setVisibility(View.VISIBLE);
                Picasso.with(getContext()).load(R.drawable.no_video).into(noVideo);
            }
            else {
                simpleExoPlayerView.setVisibility(View.VISIBLE);
                noVideo.setVisibility(View.GONE);
                initializePlayer(Uri.parse(videoUrl));
                simpleExoPlayer.setPlayWhenReady(playWhenReady);
                simpleExoPlayer.seekTo(videoPosition);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle currentState) {
        super.onSaveInstanceState(currentState);
        currentState.putString(STEPS_SELECTED,stepsId);
        currentState.putString(VIDEO_URL,videoUrl);
        currentState.putString(DESC,desc);
        currentState.putLong(VIDEO_POSITION, videoPosition);
        currentState.putBoolean(WHEN_READY, playWhenReady);
    }

    private void initializePlayer(Uri mediaUri) {
        if (simpleExoPlayer == null) {
            TrackSelection.Factory videoTrackSelectionFactory =
                    new AdaptiveTrackSelection.Factory(bandwidthMeter);
            DefaultTrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

            LoadControl loadControl = new DefaultLoadControl();
            simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(getContext(), trackSelector, loadControl);

            simpleExoPlayerView.setPlayer(simpleExoPlayer);

            MediaSource mediaSource = new ExtractorMediaSource(mediaUri,
                    new DefaultDataSourceFactory(getContext(), userAgent),
                    new DefaultExtractorsFactory(),
                    null,
                    null);
            simpleExoPlayer.prepare(mediaSource);
            simpleExoPlayer.setPlayWhenReady(true);
        }
    }
}
