package com.mike.bakingapp.ui.Fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mike.bakingapp.R;
import com.mike.bakingapp.Recipes;
import com.mike.bakingapp.adapters.RecipesAdapter;
import com.mike.bakingapp.json.JSONRecipe;
import com.mike.bakingapp.ui.Activities.MainActivity;
import com.mike.bakingapp.utils.SimpleIdlingResource;

import java.util.ArrayList;

public class RecipeFragment extends Fragment {

    public static ArrayList<Recipes> recipesList;
    @SuppressLint("StaticFieldLeak")
    public static RecipesAdapter recipesAdapter;

    public RecipeFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_master_list, container, false);
        SimpleIdlingResource idlingResource = (SimpleIdlingResource)((MainActivity)getActivity()).getIdlingResource();
        recipesList = new ArrayList<>();
        recipesAdapter = new RecipesAdapter(getContext(), recipesList);

        new JSONRecipe(idlingResource).execute();

        RecyclerView recyclerView = rootView.findViewById(R.id.recycler_view_recipes_main);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(recipesAdapter);


        if (rootView.getTag()!= null){
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(),4);
            recyclerView.setLayoutManager(gridLayoutManager);
        }
        else {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
            recyclerView.setLayoutManager(linearLayoutManager);
        }
        return rootView;
    }
}
