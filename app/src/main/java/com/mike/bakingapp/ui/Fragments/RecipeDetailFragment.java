package com.mike.bakingapp.ui.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mike.bakingapp.Ingredients;
import com.mike.bakingapp.R;
import com.mike.bakingapp.RecipeListener;
import com.mike.bakingapp.Steps;
import com.mike.bakingapp.adapters.IngredientsAdapter;
import com.mike.bakingapp.adapters.StepsAdapter;
import com.mike.bakingapp.json.JSONIngredient;
import com.mike.bakingapp.json.JSONSteps;
import com.mike.bakingapp.ui.Activities.RecipeDetailActivity;
import com.mike.bakingapp.utils.SimpleIdlingResource;
import com.mike.bakingapp.utils.Utils;
import com.mike.bakingapp.widget.RecipeWidgetUpdate;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Objects;

import static com.mike.bakingapp.utils.Static.ID;
import static com.mike.bakingapp.utils.Static.INGREDIENTS_LIST;
import static com.mike.bakingapp.utils.Static.RECIPE_NAME;

public class RecipeDetailFragment extends Fragment {

    public static final String SCROLL_POSITION = "SCROLL_POSITION";
    public static ArrayList<Ingredients> ingredientsList;
    @SuppressLint("StaticFieldLeak")
    public static IngredientsAdapter ingredientsAdapter;
    public static ArrayList<Steps> stepsList;
    @SuppressLint("StaticFieldLeak")
    public static StepsAdapter stepsAdapter;
    public static String recipeId;
    RecipeListener mCallback;
    private ScrollView mScrollView;
    private String recipeName;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mCallback = (RecipeListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnImageClickListener");
        }
    }
    public RecipeDetailFragment() {
    }

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_detail, container, false);
        Intent i= Objects.requireNonNull(getActivity()).getIntent();
        recipeId = Objects.requireNonNull(i.getExtras()).getString(ID);
        recipeName = i.getExtras().getString(RECIPE_NAME);
        Utils.setPreferences(RECIPE_NAME, recipeName, getContext());
        mScrollView = rootView.findViewById(R.id.scrollview_details);
        SimpleIdlingResource idlingResource = (SimpleIdlingResource)((RecipeDetailActivity)getActivity()).getIdlingResource();
        //JSON
        new JSONIngredient(getContext(), idlingResource).execute();
        new JSONSteps(getContext(), idlingResource).execute();

        if(savedInstanceState != null){
            final int[] scrollPosition = savedInstanceState.getIntArray(SCROLL_POSITION);
            if(scrollPosition != null)
                mScrollView.post(new Runnable() {
                    public void run() {
                        mScrollView.scrollTo(scrollPosition[0], scrollPosition[1]);
                    }
                });
        }

        //Ingredients
        ingredientsList = new ArrayList<>();
        ingredientsAdapter = new IngredientsAdapter(getContext(), ingredientsList);
        RecyclerView ingredientRecyclerView = rootView.findViewById(R.id.recycler_view_ingredients);
        ingredientRecyclerView.setHasFixedSize(true);
        ingredientRecyclerView.setAdapter(ingredientsAdapter);
        LinearLayoutManager ingredientLinearLayoutManager = new LinearLayoutManager(getContext());
        ingredientRecyclerView.setLayoutManager(ingredientLinearLayoutManager);

        //Steps
        stepsList = new ArrayList<>();
        stepsAdapter = new StepsAdapter(getContext(), stepsList, mCallback);
        RecyclerView stepsRecyclerView = rootView.findViewById(R.id.recycler_view_steps);
        stepsRecyclerView.setHasFixedSize(true);
        stepsRecyclerView.setAdapter(stepsAdapter);
        LinearLayoutManager stepsLinearLayoutManager = new LinearLayoutManager(getContext());
        stepsRecyclerView.setLayoutManager(stepsLinearLayoutManager);

        return rootView;
    }

    public static void updateWidget(Context context) {
        ArrayList<String> ingredientsWidget= new ArrayList<>();
        String recipeName =Utils.getPreferences(context, RECIPE_NAME);
        String ingredientsJSON = Utils.getPreferences(context, INGREDIENTS_LIST);

        ingredientsList = fromJson(ingredientsJSON,
                new TypeToken<ArrayList<Ingredients>>() {
                }.getType());

        for (Ingredients ingredient : ingredientsList) {
            String ingredient1 = ingredient.getIngredient();
            String measure = ingredient.getMeasure();
            String quantity = ingredient.getQuantity();
            ingredientsWidget.add(ingredient1.toUpperCase() + "\n"
            + "Measure: " + measure + "\n" + "Quantity: " + quantity);
        }

        RecipeWidgetUpdate.startWidgetUpdate(context, ingredientsWidget, recipeName);
    }

    public static ArrayList<Ingredients> fromJson(String jsonString, Type type) {
        return new Gson().fromJson(jsonString, type);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putIntArray(SCROLL_POSITION,
                new int[]{ mScrollView.getScrollX(), mScrollView.getScrollY()});
    }

    @Override
    public void onStop() {
        super.onStop();
        mCallback.onWidgetContentUpdate();
    }
}
