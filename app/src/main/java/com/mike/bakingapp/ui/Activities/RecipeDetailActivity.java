package com.mike.bakingapp.ui.Activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.test.espresso.IdlingResource;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.mike.bakingapp.R;
import com.mike.bakingapp.RecipeListener;
import com.mike.bakingapp.ui.Fragments.RecipeDetailFragment;
import com.mike.bakingapp.ui.Fragments.StepsDetailFragment;
import com.mike.bakingapp.utils.SimpleIdlingResource;

public class RecipeDetailActivity extends AppCompatActivity implements RecipeListener {
    public static boolean mTwoPane;
    private FragmentManager fragmentManager;
    private SimpleIdlingResource mIdlingResource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.container);
        fragmentManager = getSupportFragmentManager();
        if (findViewById(R.id.fragment_steps_container) != null){
            mTwoPane = true;

            if (savedInstanceState == null) {
                RecipeDetailFragment recipeFragment = new RecipeDetailFragment();
                // Add the fragment to its container using a transaction
                fragmentManager.beginTransaction()
                        .add(R.id.fragment_container, recipeFragment)
                        .commit();

                StepsDetailFragment stepsDetailFragment = new StepsDetailFragment();
                stepsDetailFragment.setStepsId("0");
                stepsDetailFragment.setDesc("");
                fragmentManager.beginTransaction()
                        .add(R.id.fragment_steps_container, stepsDetailFragment)
                        .commit();
            }
        }
        else {

            RecipeDetailFragment recipeFragment = new RecipeDetailFragment();
            // Add the fragment to its container using a transaction
            fragmentManager.beginTransaction()
                    .add(R.id.fragment_container, recipeFragment)
                    .commit();
        }
    }

    @Override
    public void onStepsClicked(String stepsId, String recipeId, String videoUrl, String desc) {
        if (mTwoPane){

            StepsDetailFragment stepsDetailFragment = new StepsDetailFragment();
            stepsDetailFragment.setRecipeId(recipeId);
            stepsDetailFragment.setStepsId(stepsId);
            stepsDetailFragment.setVideoUrl(videoUrl);
            stepsDetailFragment.setDesc(desc);
            fragmentManager.beginTransaction()
                    .replace(R.id.fragment_steps_container, stepsDetailFragment)
                    .commit();
        }
    }

    @Override
    public void onWidgetContentUpdate() {
        RecipeDetailFragment.updateWidget(RecipeDetailActivity.this);
    }

    @VisibleForTesting
    @NonNull
    public IdlingResource getIdlingResource() {
        if (mIdlingResource == null) {
            mIdlingResource = new SimpleIdlingResource();
        }
        return mIdlingResource;
    }
}
