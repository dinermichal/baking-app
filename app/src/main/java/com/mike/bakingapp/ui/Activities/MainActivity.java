package com.mike.bakingapp.ui.Activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.test.espresso.IdlingResource;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.mike.bakingapp.R;
import com.mike.bakingapp.ui.Fragments.RecipeFragment;
import com.mike.bakingapp.utils.SimpleIdlingResource;

public class MainActivity extends AppCompatActivity {


    private IdlingResource mIdlingResource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.container);
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (savedInstanceState == null) {
            RecipeFragment recipeFragment = new RecipeFragment();
            // Add the fragment to its container using a transaction
            fragmentManager.beginTransaction()
                    .add(R.id.fragment_container, recipeFragment)
                    .commit();
        }
    }
    @VisibleForTesting
    @NonNull
    public IdlingResource getIdlingResource() {
        if (mIdlingResource == null) {
            mIdlingResource = new SimpleIdlingResource();
        }
        return mIdlingResource;
    }
}
