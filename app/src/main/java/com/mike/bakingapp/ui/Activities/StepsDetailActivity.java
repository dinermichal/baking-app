package com.mike.bakingapp.ui.Activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.mike.bakingapp.R;
import com.mike.bakingapp.ui.Fragments.StepsDetailFragment;

public class StepsDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.container);
        FragmentManager fragmentManager = getSupportFragmentManager();

        if (savedInstanceState == null) {
            StepsDetailFragment stepsFragment = new StepsDetailFragment();
            // Add the fragment to its container using a transaction
            fragmentManager.beginTransaction()
                    .add(R.id.fragment_container, stepsFragment)
                    .commit();
        }
    }
}
