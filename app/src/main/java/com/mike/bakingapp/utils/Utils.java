package com.mike.bakingapp.utils;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;

import com.mike.bakingapp.data.RecipesContract;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Utils {

    public static void setPreferences(String key, String value, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String getPreferences(Context context, String key) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, "");
    }

    public static void saveToDatabase(Context context, String recipeId, String stepId, String desc, String videoUrl) {
        ContentValues cv = new ContentValues();
        cv.put(RecipesContract.RecipesEntry.RECIPES_ID, recipeId);
        cv.put(RecipesContract.RecipesEntry.RECIPES_STEP_ID, stepId);
        cv.put(RecipesContract.RecipesEntry.RECIPES_STEP_DESC, desc);
        cv.put(RecipesContract.RecipesEntry.RECIPES_STEP_VIDEOURL, videoUrl);
        context.getContentResolver().insert(RecipesContract.RecipesEntry.CONTENT_URI, cv);
    }
    public static String download(Uri jsonURL) {

        String json;

        try {
            URL url = new URL(jsonURL.toString());
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.connect();


            InputStream inputStream = httpURLConnection.getInputStream();
            StringBuilder stringBuffer = new StringBuilder();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuffer.append(line).append("\n");
            }
            json = stringBuffer.toString();
        } catch (IOException e) {
            return null;
        }

        return json;
    }
}
