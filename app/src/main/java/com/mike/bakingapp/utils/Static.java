package com.mike.bakingapp.utils;

public class Static {

    public static final String JSON_URL = "https://d17h27t6h515a5.cloudfront.net/topher/2017/May/59121517_baking/baking.json";
    public static final String VIDEO_URL = "video_url";
    public static final String DESC = "desc" ;
    public static final String ID = "id";
    public static final String RECIPE_ID = "recipe_id";
    public static final String RECIPE_NAME = "RECIPE_NAME";
    public static final String INGREDIENTS_LIST = "INGREDIENTS_LIST";
    public static final String WIDGET_ACTION = "android.appwidget.action.APPWIDGET_UPDATE";

}
