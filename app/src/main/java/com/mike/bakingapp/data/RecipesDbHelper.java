package com.mike.bakingapp.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class RecipesDbHelper extends SQLiteOpenHelper{

    private static final String DATABASE_NAME = "recipes.db";
    private static final int DATABASE_VERSION = 1;



    public RecipesDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        final String SQL_CREATE_FAVORITES_TABLE =
                "CREATE TABLE " +
                        RecipesContract.RecipesEntry.TABLE_NAME + " (" +
                        RecipesContract.RecipesEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        RecipesContract.RecipesEntry.RECIPES_ID + " TEXT NOT NULL, " +
                        RecipesContract.RecipesEntry.RECIPES_STEP_DESC + " TEXT NOT NULL, " +
                        RecipesContract.RecipesEntry.RECIPES_STEP_ID + " TEXT NOT NULL, " +
                        RecipesContract.RecipesEntry.RECIPES_STEP_VIDEOURL + " TEXT NOT NULL " +
                        "); ";

        db.execSQL(SQL_CREATE_FAVORITES_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + RecipesContract.RecipesEntry.TABLE_NAME);
        onCreate(db);
    }
}
