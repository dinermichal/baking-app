package com.mike.bakingapp.data;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

public class RecipesContract {
    public static String CONTENT_AUTHORITY = "com.mike.bakingapp";
    static Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static String PATH_RECIPES = "recipes";

    public static final class RecipesEntry implements BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_RECIPES).build();
        public static final String TABLE_NAME = "recipes";
        public static final String RECIPES_ID = "id";
        public static final String RECIPES_STEP_ID = "steps_id";
        public static final String RECIPES_STEP_DESC = "description";
        public static final String RECIPES_STEP_VIDEOURL= "videoURL";
        public static Uri buildMovieUri(long id){
            return ContentUris.withAppendedId(BASE_CONTENT_URI,id);
        }

    }
}
