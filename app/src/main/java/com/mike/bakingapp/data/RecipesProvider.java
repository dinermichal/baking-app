package com.mike.bakingapp.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;

import java.util.Objects;

public class RecipesProvider extends ContentProvider {
    public static final int RECIPES = 101;
    private static UriMatcher sUriMatcher = buildUriMatcher();
    private RecipesDbHelper mDbHelper;

    public static UriMatcher buildUriMatcher(){
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI(RecipesContract.CONTENT_AUTHORITY,
                RecipesContract.PATH_RECIPES, RECIPES);
        return matcher;
    }

    @Override
    public boolean onCreate() {
        mDbHelper = new RecipesDbHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor cursor;
        switch (sUriMatcher.match(uri)){
            case RECIPES:{
                SQLiteDatabase db = mDbHelper.getReadableDatabase();
                cursor = db.query(RecipesContract.RecipesEntry.TABLE_NAME,
                        projection,selection,selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        return cursor;
    }

    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        Uri returnUri = null;

        switch (sUriMatcher.match(uri)){
            case RECIPES:{
                SQLiteDatabase db = mDbHelper.getWritableDatabase();
                long id = db.insert(RecipesContract.RecipesEntry.TABLE_NAME,null,values);
                if (id>0){
                    returnUri =  RecipesContract.RecipesEntry.buildMovieUri(id);
                }
                break;
            }
            default:{
                throw new UnsupportedOperationException("Unknown uri: " + uri);
            }
        }
        Objects.requireNonNull(getContext()).getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        switch (sUriMatcher.match(uri)){
            case RECIPES:{
                SQLiteDatabase db = mDbHelper.getWritableDatabase();
                return db.delete(RecipesContract.RecipesEntry.TABLE_NAME,selection,selectionArgs);
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}