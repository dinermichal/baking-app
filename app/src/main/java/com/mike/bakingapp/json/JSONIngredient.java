package com.mike.bakingapp.json;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.mike.bakingapp.Ingredients;
import com.mike.bakingapp.ui.Fragments.RecipeDetailFragment;
import com.mike.bakingapp.utils.SimpleIdlingResource;
import com.mike.bakingapp.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.mike.bakingapp.ui.Fragments.RecipeDetailFragment.ingredientsList;
import static com.mike.bakingapp.utils.Static.INGREDIENTS_LIST;
import static com.mike.bakingapp.utils.Static.JSON_URL;
import static com.mike.bakingapp.utils.Utils.download;

public class JSONIngredient extends AsyncTask<Void,Void,String> {

    private static SimpleIdlingResource idlingResource;
    private static Context context;


    public JSONIngredient(Context context, SimpleIdlingResource idlingResource) {
        JSONIngredient.idlingResource = idlingResource;
        JSONIngredient.context = context;
    }
    @Override
    protected String doInBackground(Void... voids) {


        Uri builtUri = Uri.parse(JSON_URL);

        String response;
        try {
            response  = download(builtUri);
            return response;
        }catch (Exception e){
            return null;
        }
    }

    @Override
    protected void onPostExecute(String response) {
        if (response != null) {
            parse(response);
        }
        if (idlingResource != null) {
            idlingResource.setIdleState(true);
        }

    }

    private static void parse(String jsonString)
    {
        if (idlingResource != null) {
            idlingResource.setIdleState(false);
        }
        ingredientsList.clear();
        try
        {
            if(!TextUtils.isEmpty(jsonString)) {
                JSONArray jsonArray = new JSONArray(jsonString);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    String id1 = jsonObject.getString("id");
                    if (id1.equals(RecipeDetailFragment.recipeId)) {
                        JSONArray mArray = jsonObject.getJSONArray("ingredients");
                        for (int x = 0; x < mArray.length(); x++) {
                            JSONObject ingredients = mArray.getJSONObject(x);
                            Ingredients ingredients1 = new Ingredients();
                            String quantity = ingredients.getString("quantity");
                            String measure = ingredients.getString("measure");
                            String ingredient = ingredients.getString("ingredient");
                            ingredients1.setQuantity(quantity);
                            ingredients1.setMeasure(measure);
                            ingredients1.setIngredient(ingredient);
                            ingredientsList.add(ingredients1);
                            RecipeDetailFragment.ingredientsAdapter.notifyDataSetChanged();
                        }

                    }
                }
                setList(INGREDIENTS_LIST, ingredientsList);
            }
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
    }

    private static <T> void setList(String key, ArrayList<T> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);

        Utils.setPreferences(key, json, context);

    }
}
