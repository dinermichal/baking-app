package com.mike.bakingapp.json;

import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.mike.bakingapp.Recipes;
import com.mike.bakingapp.ui.Fragments.RecipeFragment;
import com.mike.bakingapp.utils.SimpleIdlingResource;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.mike.bakingapp.utils.Static.JSON_URL;
import static com.mike.bakingapp.utils.Utils.download;

public class JSONRecipe extends AsyncTask<Void,Void,String> {

    private static SimpleIdlingResource idlingResource;

    public JSONRecipe(SimpleIdlingResource idlingResource) {
        JSONRecipe.idlingResource = idlingResource;
    }
    @Override
    protected String doInBackground(Void... voids) {


        Uri builtUri = Uri.parse(JSON_URL);

        String response;
        try {
            response  = download(builtUri);
            return response;
        }catch (Exception e){
            return null;
        }

    }

    @Override
    protected void onPostExecute(String response) {
        if (response != null) {
            parse(response);
        }
        if (idlingResource != null) {
            idlingResource.setIdleState(true);
        }
    }

    private static void parse(String jsonString)
    {
        if (idlingResource != null) {
            idlingResource.setIdleState(false);
        }
        RecipeFragment.recipesList.clear();
        try
        {
            if(!TextUtils.isEmpty(jsonString)) {


                JSONArray jsonArray = new JSONArray(jsonString);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    String name = jsonObject.getString("name");
                    String id = jsonObject.getString("id");
                    String servings = jsonObject.getString("servings");
                    Recipes recipes = new Recipes();
                    recipes.setName(name);
                    recipes.setId(id);
                    recipes.setServings(servings);
                    RecipeFragment.recipesList.add(recipes);
                    RecipeFragment.recipesAdapter.notifyDataSetChanged();
                }
            }

        } catch (JSONException e1) {
            e1.printStackTrace();
        }
    }
}
