package com.mike.bakingapp.json;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.mike.bakingapp.Steps;
import com.mike.bakingapp.ui.Fragments.RecipeDetailFragment;
import com.mike.bakingapp.utils.SimpleIdlingResource;
import com.mike.bakingapp.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.mike.bakingapp.utils.Static.JSON_URL;
import static com.mike.bakingapp.utils.Utils.download;

public class JSONSteps extends AsyncTask<Void,Void,String> {

    @SuppressLint("StaticFieldLeak")
    private static Context context;
    private static SimpleIdlingResource idlingResource;

    public JSONSteps(Context context, SimpleIdlingResource idlingResource) {
        JSONSteps.context = context;
        JSONSteps.idlingResource = idlingResource;
    }
    @Override
    protected String doInBackground(Void... voids) {


        Uri builtUri = Uri.parse(JSON_URL);

        String response;
        try {
            response  = download(builtUri);
            return response;
        }catch (Exception e){
            return null;
        }
    }

    @Override
    protected void onPostExecute(String response) {
        if (response != null) {
            parse(response);
        }
        if (idlingResource != null) {
            idlingResource.setIdleState(true);
        }
    }

    private static void parse(String jsonString)
    {
        if (idlingResource != null) {
            idlingResource.setIdleState(false);
        }
        RecipeDetailFragment.stepsList.clear();
        try
        {
            if(!TextUtils.isEmpty(jsonString)) {
                JSONArray jsonArray = new JSONArray(jsonString);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    String recipeId = jsonObject.getString("id");
                    if (recipeId.equals(RecipeDetailFragment.recipeId)) {
                        JSONArray mArray = jsonObject.getJSONArray("steps");
                        for (int x = 0; x < mArray.length(); x++) {
                            JSONObject steps = mArray.getJSONObject(x);
                            Steps steps1 = new Steps();
                            String stepId = steps.getString("id");
                            String shortDesc = steps.getString("shortDescription");
                            String desc = steps.getString("description");
                            String videoUrl = steps.getString("videoURL");
                            String thumbUrl = steps.getString("thumbnailURL");
                            steps1.setId(stepId);
                            steps1.setShortDesc(shortDesc);
                            steps1.setDesc(desc);
                            if (TextUtils.isEmpty(videoUrl)){
                                if (!TextUtils.isEmpty(thumbUrl)) {
                                    steps1.setVideoUrl(thumbUrl);
                                    Utils.saveToDatabase(context, RecipeDetailFragment.recipeId, stepId, desc, thumbUrl);
                                }
                                else{
                                    Utils.saveToDatabase(context, RecipeDetailFragment.recipeId, stepId, desc, "");
                                }
                            }
                            else{
                                steps1.setVideoUrl(videoUrl);
                                Utils.saveToDatabase(context, RecipeDetailFragment.recipeId, stepId, desc, videoUrl);
                            }
                            RecipeDetailFragment.stepsList.add(steps1);
                            RecipeDetailFragment.stepsAdapter.notifyDataSetChanged();
                        }
                    }
                }
            }
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
    }
}
