package com.mike.bakingapp;

public interface RecipeListener {
     void onStepsClicked(String stepsId, String recipeId, String videoUrl, String desc);
     void onWidgetContentUpdate();
}
